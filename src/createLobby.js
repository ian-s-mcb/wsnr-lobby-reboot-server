import { WebSocketServer } from 'ws'

import {
  announceUserConnected, announceUserDisconnected
} from './messageUtils'
import {
  STATUS_AVAIL, EVENT_CONN, EVENT_CLOSE, EVENT_MSG
} from './constants'

import { handleMessage } from './handleMessage'

export function createLobby (server) {
  const wss = new WebSocketServer({ server })

  wss.on(EVENT_CONN, ws => {
    // Store client properties in ws object
    Object.assign(ws, {
      name: '',
      status: STATUS_AVAIL
    })
    announceUserConnected(wss, ws)

    ws.on(EVENT_MSG, message => {
      handleMessage(wss, ws, message)
    })
    ws.on(EVENT_CLOSE, message => {
      announceUserDisconnected(wss, ws)
    })
  })
}
