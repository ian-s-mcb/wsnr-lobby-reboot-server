import {
  ACCEPT_RENAME, ACCEPT_UPDATE_STATUS
} from './constants'
import { rename, updateStatus } from './lobbyMessages'

export function handleMessage (wss, ws, message) {
  const data = JSON.parse(message.toString())

  switch (data.type) {
    case ACCEPT_RENAME:
      rename(wss, ws, data)
      break
    case ACCEPT_UPDATE_STATUS:
      updateStatus()
      break
    default:
      console.log('Invalid message type')
      break
  }
}
