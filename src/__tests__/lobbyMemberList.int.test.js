import {
  startLobby, waitForSocketState, createClient
} from '../testUtils'

const port = 3000 + Number(process.env.JEST_WORKER_ID)

describe('Lobby', () => {
  let server

  beforeAll(async () => {
    server = await startLobby(port)
  })

  afterAll(() => server.close())

  test('emits user connected', async () => {
    // Create socket, close after 1 message
    const [client, messages] = await createClient(port, 1)
    await waitForSocketState(client, client.CLOSED)

    // Check message count, message type
    expect(messages.length).toBe(1)
    const message = messages[0]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.title).toBe('An AnonUser connected')
  })

  test('detects one named user', async () => {
    // Create socket, close after 1 message
    const [client, receivedMessages] = await createClient(port, 2)
    await waitForSocketState(client, client.OPEN)
    const newName = 'abby'
    const emittedMessage = JSON.stringify({
      type: 'RENAME',
      newName
    })
    client.send(emittedMessage)
    await waitForSocketState(client, client.CLOSED)

    // Check message count
    expect(receivedMessages.length).toBe(2)
    // Skip first message, check second message
    const message = receivedMessages[1]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(newName)
    expect(message.title).toBe(`AnonUser renamed to ${newName}`)
    expect(message.members).toStrictEqual([{
      name: newName,
      status: 'AVAILABLE'
    }])
  })

  test('detects two named users', async () => {
    // Build rename messages, expected members arrays
    let message
    const client1Name = 'abby'
    const emittedMessage1 = JSON.stringify({
      type: 'RENAME',
      newName: client1Name
    })
    const client2Name = 'barbara'
    const emittedMessage2 = JSON.stringify({
      type: 'RENAME',
      newName: client2Name
    })
    const members1 = [{
      name: client1Name,
      status: 'AVAILABLE'
    }]
    const members2 = [
      {
        name: client1Name,
        status: 'AVAILABLE'
      },
      {
        name: client2Name,
        status: 'AVAILABLE'
      }
    ]

    // Start client1, emit rename
    const [client1, receivedMessages1] = await createClient(port, 4)
    await waitForSocketState(client1, client1.OPEN)
    client1.send(emittedMessage1)

    // Start client2, emit rename
    const [client2, receivedMessages2] = await createClient(port, 2)
    await waitForSocketState(client2, client2.OPEN)
    client2.send(emittedMessage2)

    // Let clients close
    await waitForSocketState(client1, client1.CLOSED)
    await waitForSocketState(client2, client2.CLOSED)

    // Check client1 messages
    // Check message count
    expect(receivedMessages1.length).toBe(4)
    // Skip first message (covered by prior test)
    // Check second message
    message = receivedMessages1[1]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client1Name)
    expect(message.title).toBe(`AnonUser renamed to ${client1Name}`)
    expect(message.members).toStrictEqual(members1)
    // Skip third message (covered by prior test)
    // Check fourth message
    message = receivedMessages1[3]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client2Name)
    expect(message.title).toBe(`AnonUser renamed to ${client2Name}`)
    expect(message.members).toStrictEqual(members2)

    // Check client2 messages
    // Check message count
    expect(receivedMessages2.length).toBe(2)
    // Skip first message (covered by prior test)
    // Check second message
    message = receivedMessages2[1]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client2Name)
    expect(message.title).toBe(`AnonUser renamed to ${client2Name}`)
    expect(message.members).toStrictEqual(members2)
  })

  test('detects when user exits', async () => {
    // Build rename messages, expected members arrays
    let message
    const client1Name = 'abby'
    const emittedMessage1 = JSON.stringify({
      type: 'RENAME',
      newName: client1Name
    })
    const client2Name = 'barbara'
    const emittedMessage2 = JSON.stringify({
      type: 'RENAME',
      newName: client2Name
    })
    const members1 = [{
      name: client1Name,
      status: 'AVAILABLE'
    }]
    const members2 = [
      {
        name: client1Name,
        status: 'AVAILABLE'
      },
      {
        name: client2Name,
        status: 'AVAILABLE'
      }
    ]

    // Start client1, emit rename
    const [client1, receivedMessages1] = await createClient(port, 5)
    await waitForSocketState(client1, client1.OPEN)
    client1.send(emittedMessage1)

    // Start client2, emit rename
    const [client2, receivedMessages2] = await createClient(port, 2)
    await waitForSocketState(client2, client2.OPEN)
    client2.send(emittedMessage2)

    // Let clients close
    await waitForSocketState(client1, client1.CLOSED)
    await waitForSocketState(client2, client2.CLOSED)

    // Check client1 messages
    // Check message count
    expect(receivedMessages1.length).toBe(5)
    // Skip first message (covered by prior test)
    // Check third message
    message = receivedMessages1[1]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client1Name)
    expect(message.title).toBe(`AnonUser renamed to ${client1Name}`)
    expect(message.members).toStrictEqual(members1)
    // Check fourth message
    message = receivedMessages1[3]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client2Name)
    expect(message.title).toBe(`AnonUser renamed to ${client2Name}`)
    expect(message.members).toStrictEqual(members2)
    // Check fifth message
    message = receivedMessages1[4]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client2Name)
    expect(message.title).toBe(`${client2Name} disconnected`)
    expect(message.members).toStrictEqual(members1)

    // Check client2 messages
    // Check message count
    expect(receivedMessages2.length).toBe(2)
    // Skip first message (covered by prior test)
    // Check second message
    message = receivedMessages2[1]
    expect(message.type).toBe('UPDATE_MEMBER_LIST')
    expect(message.sender).toBe(client2Name)
    expect(message.title).toBe(`AnonUser renamed to ${client2Name}`)
    expect(message.members).toStrictEqual(members2)
  })
})
