import http from 'http'
import WebSocket from 'ws'
import { createLobby } from './createLobby'

function startLobby (port) {
  const server = http.createServer()
  createLobby(server)

  return new Promise((resolve) => {
    server.listen(port, () => resolve(server))
  })
}

function waitForSocketState (socket, state) {
  return new Promise(function (resolve) {
    setTimeout(function () {
      if (socket.readyState === state) {
        resolve()
      } else {
        waitForSocketState(socket, state).then(resolve)
      }
    })
  })
}

async function createClient (port, closeAfter) {
  const client = new WebSocket(`ws://localhost:${port}`)
  // await waitForSocketState(client, client.OPEN)
  const messages = []

  client.on('message', (data) => {
    messages.push(JSON.parse(data.toString()))

    if (messages.length === closeAfter) {
      client.close()
    }
  })

  return [client, messages]
}

export { startLobby, waitForSocketState, createClient }
