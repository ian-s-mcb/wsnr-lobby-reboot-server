import http from 'http'

import { createLobby } from './createLobby'

const PORT = process.env.PORT || 5000
const server = http.createServer()
createLobby(server)
server.listen(PORT, () => console.log(`Listening on port ${PORT}`))
