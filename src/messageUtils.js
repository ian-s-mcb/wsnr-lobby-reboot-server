import { EMIT_UPDATE_MEM_LIST } from './constants'

function announceUserConnected (wss, ws) {
  buildAndSendMessage(
    wss,
    ws,
    wss.clients,
    'An AnonUser connected',
    EMIT_UPDATE_MEM_LIST
  )
}

function announceUserDisconnected (wss, ws) {
  const name = ws.name || 'AnonUser'
  buildAndSendMessage(
    wss,
    ws,
    wss.clients,
    `${name} disconnected`,
    EMIT_UPDATE_MEM_LIST
  )
}

// Send recipients a newly built message
function buildAndSendMessage (wss, ws, recipients, title, type) {
  const members = buildMembers([...wss.clients])
  const message = buildMessage(ws, title, type, members)
  sendMessage(recipients, message)
}

// Build a message from client properties
function buildMessage (ws, title, type, members) {
  const {
    name,
    status
  } = ws

  const message = {
    members,
    sender: name,
    status,
    title,
    type
  }

  return message
}

// Members array contains the name and status of clients with names
const buildMembers = clients => (
  clients
    .filter(c => c.name)
    .map(c => ({
      name: c.name,
      status: c.status
    }))
)

// Send recipients the given message
function sendMessage (recipients, message) {
  recipients
    .forEach(r => {
      message.recipient = r.name
      r.send(JSON.stringify(message))
    })
}

export {
  announceUserConnected,
  announceUserDisconnected,
  buildAndSendMessage
}
