import { EMIT_UPDATE_MEM_LIST } from './constants'
import { buildAndSendMessage } from './messageUtils'

function rename (wss, ws, m) {
  // Ignore if new name is absent or identical to name
  if (!m.newName || (ws.name === m.newName)) { return }

  const originalName = ws.name || 'AnonUser'
  ws.name = m.newName
  const title = `${originalName} renamed to ${ws.name}`
  const type = EMIT_UPDATE_MEM_LIST

  buildAndSendMessage(wss, ws, wss.clients, title, type)
}

function updateStatus () {}

export { rename, updateStatus }
